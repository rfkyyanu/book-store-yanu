package com.example.bookstore.controller;

import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Book;
import com.example.bookstore.repository.BookRepository;

@RestController
@RequestMapping("/api/book")
public class BookController {
	
	@Autowired
	BookRepository bookRepository;
	
	// Show all book
	@GetMapping("/show/all")
	public HashMap<String, Object> getAllBook() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("Message", "Read Success");
		result.put("Total", bookRepository.findAll().size());
		result.put("Data", bookRepository.findAll());
		
		return result;
	}
	
	// Create new book
	@PostMapping("/create")
	public HashMap<String, Object> createBook(@Valid @RequestBody Book book) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("Message", "Create Success");
		result.put("Data", bookRepository.save(book));
		
		return result;
	}
	
	// Update existed book
	@PutMapping("/update")
	public HashMap<String, Object> updateBook(@RequestParam(name = "id") Long bookId, @Valid @RequestBody Book bookDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Book updatedBook = bookRepository.findById(bookId).get();
			
		if (bookDetails.getTitle() != null) {
			updatedBook.setTitle(bookDetails.getTitle());
		} else {
			updatedBook.setTitle(updatedBook.getTitle());
		}
		
		if (bookDetails.getReleaseDate() != null) {
			updatedBook.setReleaseDate(bookDetails.getReleaseDate());
		} else {
			updatedBook.setReleaseDate(updatedBook.getReleaseDate());
		}
		
		if (bookDetails.getPrice() != null) {
			updatedBook.setPrice(bookDetails.getPrice());
		} else {
			updatedBook.setPrice(updatedBook.getPrice());
		}
		
		if (bookDetails.getAuthor() != null) {
			updatedBook.setAuthor(bookDetails.getAuthor());
		} else {
			updatedBook.setAuthor(updatedBook.getAuthor());
		}
		
		if (bookDetails.getPublisher() != null) {
			updatedBook.setPublisher(bookDetails.getPublisher());
		} else {
			updatedBook.setPublisher(updatedBook.getPublisher());
		}
			
		result.put("Message", "Update Success");
		result.put("Total", result.size());
		result.put("Data", bookRepository.save(updatedBook));
		
		return result;
	}
	
	// Delete existed Book
	@DeleteMapping("/delete")
	public HashMap<String, Object> deleteBook(@RequestParam(name = "id") Long bookId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Book deletedBook = bookRepository.findById(bookId).get();
		
		bookRepository.delete(deletedBook);

		result.put("Message", "Delete Success");
		
		return result;
	}
	
}
