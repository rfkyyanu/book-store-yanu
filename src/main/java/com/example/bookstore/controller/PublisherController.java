package com.example.bookstore.controller;

import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Publisher;
import com.example.bookstore.repository.PublisherRepository;

@RestController
@RequestMapping("api/publisher")
public class PublisherController {
	
	@Autowired
	PublisherRepository publisherRepository;
	
	// Show all publisher
	@GetMapping("/show/all")
	public HashMap<String, Object> getAllPublisher() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("Message", "Read Success");
		result.put("Total", publisherRepository.findAll().size());
		result.put("Data", publisherRepository.findAll());
		
		return result;
	}
	
	// Create new publisher
	@PostMapping("/create")
	public HashMap<String, Object> createPublisher(@Valid @RequestBody Publisher publisher) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("Message", "Create Success");
		result.put("Data", publisherRepository.save(publisher));
		
		return result;
	}
	
	// Update existed publisher
	@PutMapping("/update")
	public HashMap<String, Object> updatePublisher(@RequestParam(name = "id") Long publisherId, @Valid @RequestBody Publisher publisherDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Publisher updatedPublisher = publisherRepository.findById(publisherId).get();
			
		if (publisherDetails.getCompanyName() != null) {
			updatedPublisher.setCompanyName(publisherDetails.getCompanyName());
		} else {
			updatedPublisher.setCompanyName(updatedPublisher.getCompanyName());
		}
		
		if (publisherDetails.getCountry() != null) {
			updatedPublisher.setCountry(publisherDetails.getCountry());
		} else {
			updatedPublisher.setCountry(updatedPublisher.getCountry());
		}
		
		result.put("Message", "Update Success");
		result.put("Total", result.size());
		result.put("Data", publisherRepository.save(updatedPublisher));
		
		return result;
	}
	
	// Delete existed publisher
	@DeleteMapping("/delete")
	public HashMap<String, Object> deletePublisher(@RequestParam(name = "id") Long publisherId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Publisher deletedPublisher = publisherRepository.findById(publisherId).get();
		
		publisherRepository.delete(deletedPublisher);

		result.put("Message", "Delete Success");
		
		return result;
	}
	
}
