package com.example.bookstore.controller;

import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.PaperQuality;
import com.example.bookstore.repository.PaperQualityRepository;

@RestController
@RequestMapping("api/paper")
public class PaperQualityController {
	
	@Autowired
	PaperQualityRepository paperRepository;
	
	// Show all paper quality
	@GetMapping("/show/all")
	public HashMap<String, Object> getAllPaper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("Message", "Read Success");
		result.put("Total", paperRepository.findAll().size());
		result.put("Data", paperRepository.findAll());
		
		return result;
	}
	
	// Create new paper quality
	@PostMapping("/create")
	public HashMap<String, Object> createPaper(@Valid @RequestBody PaperQuality paper) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("Message", "Create Success");
		result.put("Data", paperRepository.save(paper));
		
		return result;
	}
	
	// Update existed paper quality
	@PutMapping("/update")
	public HashMap<String, Object> updatePaper(@RequestParam(name = "id") Long paperId, @Valid @RequestBody PaperQuality paperDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		PaperQuality updatedPaper = paperRepository.findById(paperId).get();
			
		if (paperDetails.getQualityName() != null) {
			updatedPaper.setQualityName(paperDetails.getQualityName());
		} else {
			updatedPaper.setQualityName(updatedPaper.getQualityName());
		}
		
		if (paperDetails.getPaperPrice() != null) {
			updatedPaper.setPaperPrice(paperDetails.getPaperPrice());
		} else {
			updatedPaper.setPaperPrice(updatedPaper.getPaperPrice());
		}
		
		result.put("Message", "Update Success");
		result.put("Total", result.size());
		result.put("Data", paperRepository.save(updatedPaper));
		
		return result;
	}
	
	// Delete existed paper quality
	@DeleteMapping("/delete")
	public HashMap<String, Object> deletePaper(@RequestParam(name = "id") Long paperId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		PaperQuality deletedPaper = paperRepository.findById(paperId).get();
		
		paperRepository.delete(deletedPaper);

		result.put("Message", "Delete Success");
		
		return result;
	}
	
}
