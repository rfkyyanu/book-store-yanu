package com.example.bookstore.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "paper_quality")
@JsonIgnoreProperties(value = "publishers")
public class PaperQuality {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String qualityName;

	@Column(nullable = false)
	private Double paperPrice;
	
	@OneToMany(mappedBy = "paperQuality", cascade = CascadeType.ALL)
	private Set<Publisher> publishers;
	
	public PaperQuality() {
		
	}
	
	public PaperQuality(String qualityName, Double paperPrice) {
		super();
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public Double getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(Double paperPrice) {
		this.paperPrice = paperPrice;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
	}
	
	
}
