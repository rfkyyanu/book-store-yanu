package com.example.bookstore.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "publisher")
@JsonIgnoreProperties(value = "books")
public class Publisher {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String companyName;
	
	@Column(nullable = false)
	private String country;
	
	@ManyToOne
	@JoinColumn
	private PaperQuality paperQuality;

	@OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<Book> books;
	
	public Publisher() {
		
	}
	
	public Publisher(String companyName, String country) {
		super();
		this.companyName = companyName;
		this.country = country;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public PaperQuality getPaperQuality() {
		return paperQuality;
	}

	public void setPaperQuality(PaperQuality paperQuality) {
		this.paperQuality = paperQuality;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

}
