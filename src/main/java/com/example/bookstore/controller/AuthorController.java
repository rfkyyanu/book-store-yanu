package com.example.bookstore.controller;

import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Author;
import com.example.bookstore.repository.AuthorRepository;

@RestController
@RequestMapping("api/author")
public class AuthorController {
	
	@Autowired
	AuthorRepository authorRepository;
	
	// Show all author
	@GetMapping("/show/all")
	public HashMap<String, Object> getAllAuthor() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("Message", "Read Success");
		result.put("Total", authorRepository.findAll().size());
		result.put("Data", authorRepository.findAll());
		
		return result;
	}
	
	// Create new author
	@PostMapping("/create")
	public HashMap<String, Object> createAuthor(@Valid @RequestBody Author author) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("Message", "Create Success");
		result.put("Data", authorRepository.save(author));
		
		return result;
	}
	
	// Update existed author
	@PutMapping("/update")
	public HashMap<String, Object> updateAuthor(@RequestParam(name = "id") Long authorId, @Valid @RequestBody Author authorDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Author updatedAuthor = authorRepository.findById(authorId).get();
			
		if (authorDetails.getFirstName() != null) {
			updatedAuthor.setFirstName(authorDetails.getFirstName());
		} else {
			updatedAuthor.setFirstName(updatedAuthor.getFirstName());
		}
		
		if (authorDetails.getLastName() != null) {
			updatedAuthor.setLastName(authorDetails.getLastName());
		} else {
			updatedAuthor.setLastName(updatedAuthor.getLastName());
		}
		
		if (authorDetails.getAge() != null) {
			updatedAuthor.setAge(authorDetails.getAge());
		} else {
			updatedAuthor.setAge(updatedAuthor.getAge());
		}
		
		if (authorDetails.getRating() != null) {
			updatedAuthor.setRating(authorDetails.getRating());
		} else {
			updatedAuthor.setRating(updatedAuthor.getRating());
		}
		
		if (authorDetails.getGender() != null) {
			updatedAuthor.setGender(authorDetails.getGender());
		} else {
			updatedAuthor.setGender(updatedAuthor.getGender());
		}
		
		if (authorDetails.getCountry() != null) {
			updatedAuthor.setCountry(authorDetails.getCountry());
		} else {
			updatedAuthor.setCountry(updatedAuthor.getCountry());
		}
		
		result.put("Message", "Update Success");
		result.put("Total", result.size());
		result.put("Data", authorRepository.save(updatedAuthor));
		
		return result;
	}
	
	// Delete existed author
	@DeleteMapping("/delete")
	public HashMap<String, Object> deleteAuthor(@RequestParam(name = "id") Long authorId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Author deletedAuthor = authorRepository.findById(authorId).get();
		
		authorRepository.delete(deletedAuthor);

		result.put("Message", "Delete Success");
		
		return result;
	}
	
}
